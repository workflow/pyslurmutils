# CHANGELOG.md

## Unreleased

## 0.3.4

Bug fixes:

- Fix bug in handling remote Slurm exceptions.
- Fix `lime_limit` pydantic validation warning.

## 0.3.3

Bug fixes:

- The SLURM `environment` variable remains a dictionary for the `pyslurmutils`
  API eventhough SLURM expects a list.

## 0.3.2

Bug fixes:

- Switch the `pytest_plugin` fixtures back to "session" scope.

## 0.3.1

Breaking Changes:

- Support Slurm API versions `v0.0.39`, `v0.0.40` and `v0.0.41`.
- Drop support for Slurm API version `v0.0.37`.

New features:

- Add `SlurmRestFuture.cancel_job` to cancel the SLURM job instead of the future itself.

## 0.2.0

Changes:

- Refactor the TCP/file job communication
- Remove `SlurmPythonJobRestClient`
- `SlurmRestExecutor` is now derived from `concurrent.futures.Executor`

New features:

- `SlurmRestExecutor` allows for multiple tasks per Slurm job
- `SlurmRestExecutor` allows for jobs to be kept alive automatically
- Local log level applied remotely
- Remote logs red-directed locally with `SlurmRestExecutor`

## 0.1.3

Bug fixes:

- Preserve remote traceback of chained exceptions

## 0.1.2

Changes:

- File based connection with job: refresh filesystem cache (e.g. NFS) when waiting for the result file
- TCP based connection with job: try fixed port range first

## 0.1.1

Changes:

- Check whether CLI default log directory exists

## 0.1.0

Added:

- SlurmBaseRestClient: exposes the SLURM REST API in python
- SlurmScriptRestClient: submit and monitor scripts
- SlurmPythonJobRestClient: submit and monitor python functions
- SlurmRestExecutor: pool similar to `concurrent.futures` for SLURM
- pyslurmutils: CLI for SLURM job submitting, monitoring and cancelling
