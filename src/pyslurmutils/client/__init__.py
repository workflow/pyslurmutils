"""SLURM REST clients"""

from .rest import SlurmBaseRestClient  # noqa F401
from .rest import SlurmScriptRestClient  # noqa F401
from .rest import SlurmPyConnRestClient  # noqa F401
