"""Generate code in `pyslurmutils.client.rest.api.models`"""

import os
import re
import json
from collections import defaultdict, OrderedDict
from typing import get_origin, get_args
from typing import Dict, Any, List, Tuple, Optional, Type, Union


from pydantic import Field
from pydantic import BaseModel
from pydantic import create_model

from datamodel_code_generator import PythonVersion
from datamodel_code_generator import DataModelType
from datamodel_code_generator.model import get_data_model_types
from datamodel_code_generator.parser.jsonschema import JsonSchemaParser

# Only import the API, nothing else from pyslurmutils!
from pyslurmutils.client.rest.api import slurm_access

_VersionType = Tuple[int, int, int]
_ModelTypeMap = Dict[str, Tuple[Type[BaseModel], Optional[str]]]


def resolve_schema_type(
    type_name: str,
    schema: Dict[str, Any],
    openapi_spec: Dict[str, Any],
    model_classes: _ModelTypeMap,
    title: Optional[str] = None,
    force_optional: bool = False,
) -> Union[
    Type[str],
    Type[int],
    Type[float],
    Type[bool],
    Type[List],
    Type[Dict],
    Type[BaseModel],
]:
    """
    Resolve the schema type to a Python type.

    :param type_name: Type name.
    :param schema: The schema definition to resolve.
    :param openapi_spec: The full OpenAPI spec used to resolve references.
    :param model_classes: A dictionary of Pydantic model classes.
    :param title: Preferred title.
    :param force_optional: When True all fields will be optional recursively.
    :return: The corresponding Python type (e.g., str, int, List).
    """
    schema_type = schema.get("type")
    if schema_type == "string":
        return str
    elif schema_type == "integer":
        return int
    elif schema_type == "number":
        return float
    elif schema_type == "boolean":
        return bool
    elif schema_type == "array":
        item_schema = schema.get("items", {})
        type_name = type_name + create_type_name("item")
        return List[
            resolve_schema_type(
                type_name,
                item_schema,
                openapi_spec,
                model_classes,
                force_optional=force_optional,
            )
        ]
    elif "$ref" in schema:
        keys = schema["$ref"].split("/")
        model_schema = openapi_spec
        for key in keys[1:]:  # skip the "#" item
            model_schema = model_schema[key]
        type_name = create_type_name(*key.split("_"))
        title = title or schema.get("description", None)
        return resolve_schema_type(
            type_name,
            model_schema,
            openapi_spec,
            model_classes,
            title=title,
            force_optional=force_optional,
        )
    elif schema_type == "object" or not schema_type:
        if type_name in model_classes:
            model_class, _ = model_classes[type_name]
            return model_class

        properties = schema.get("properties", {})
        required = schema.get("required", [])
        fields = {}

        for field_name, field_schema in properties.items():
            field_type_name = type_name + create_type_name(*field_name.split("_"))
            field_type = resolve_schema_type(
                field_type_name,
                field_schema,
                openapi_spec,
                model_classes,
                force_optional=force_optional,
            )

            field_title = field_schema.get("description", None)
            if not field_title:
                field_title = get_type_title(field_type, model_classes)

            if field_name in required and not force_optional and field_name:
                field = Field(..., title=field_title)
            else:
                field = Field(None, title=field_title)

            fields[field_name] = (field_type, field)

        if not fields:
            return Dict

        model_class = create_model(type_name, **fields)

        title = schema.get("description", title)
        if not title:
            title = get_model_title(model_class, model_classes)

        model_classes[type_name] = (model_class, title)
        return model_class
    else:
        raise NotImplementedError(schema_type)


def get_type_title(
    value_type: Type[Any], model_classes: _ModelTypeMap
) -> Optional[str]:
    """
    Description of any type when possible.

    :param value_type: A python class.
    :param model_classes: A dictionary of Pydantic model classes.
    :return: The type description.
    """
    if is_model(value_type):
        return get_model_title(value_type, model_classes)
    if get_origin(value_type) is list:
        value_type = get_args(value_type)[0]
        item_title = get_type_title(value_type, model_classes)
        if item_title:
            return f"List of {item_title.lower()}"


def get_model_title(
    model_class: Type[BaseModel], model_classes: _ModelTypeMap
) -> Optional[str]:
    """
    Description of a Pydantic model class.

    :param model_class: A Pydantic model class.
    :param model_classes: A dictionary of Pydantic model classes.
    :return: The model description.
    """
    if model_class.__name__ in model_classes:
        _, description = model_classes[model_class.__name__]
        if description:
            return description
    return normalize_title(model_class.__name__)


def normalize_title(string: str) -> str:
    """
    Normalize string to be a title

    :param string:
    :return: normalized title
    """
    return re.sub(r"(?<!^)(?=[A-Z])", " ", string).capitalize()


def is_model(value: Any) -> bool:
    try:
        return issubclass(value, BaseModel)
    except TypeError:
        return False


def create_type_name(*parts) -> str:
    type_name = "".join([re.sub("[^a-zA-Z0-9]", "", s).capitalize() for s in parts])
    type_name = re.sub(r"V\d+", "", type_name)
    return type_name


def find_file_for_version(
    version: _VersionType,
    version_file_map: Dict[Tuple[_VersionType, _VersionType], str],
) -> str:
    """
    Find the OpenAPI file corresponding to a given version. The order
    of `version_file_map` is important for versions that are described
    in multiple files.

    :param version: The version to find the OpenAPI file for.
    :param version_file_map: An ordered dictionary mapping version ranges to OpenAPI file paths.
    :return: The file path for the OpenAPI specification corresponding to the version.
    :raises RuntimeError: If no file is found for the version.
    """
    for (start_version, end_version), file_path in version_file_map.items():
        if start_version <= version <= end_version:
            return file_path
    raise RuntimeError(f"No OpenAPI file found for version {version}.")


def generate_model_code(json_schema: Dict[str, Any]) -> str:
    """
    Generate python code from JSON schema.

    :param json_schema: JSON schema from a Pydantic model.
    :return: python source code
    """
    data_model_types = get_data_model_types(
        DataModelType.PydanticV2BaseModel, target_python_version=PythonVersion.PY_39
    )
    parser = JsonSchemaParser(
        json_schema,
        data_model_type=data_model_types.data_model,
        data_model_root_type=data_model_types.root_model,
        data_model_field_type=data_model_types.field_model,
        data_type_manager_type=data_model_types.data_type_manager,
        dump_resolve_reference_action=data_model_types.dump_resolve_reference_action,
        use_double_quotes=True,
    )
    return parser.parse()


def endpoint_parameters_to_model(
    model_name: str,
    parameters: List[Dict],
    param_type: str,
    openapi_spec: Dict[str, Any],
    model_classes: _ModelTypeMap,
) -> Type[BaseModel]:
    """
    Create a Pydantic model class from the parameters of an endpoint.

    :param model_name: Pydantic model class name.
    :param parameters: List of endpoint parameters from the OpenAPI spec.
    :param param_type: Parameter type.
    :param openapi_spec: The OpenAPI data containing the endpoint information.
    :param model_classes: A dictionary of Pydantic model classes.
    :return: A Pydantic model class.
    """
    fields: Dict[str, Tuple[str, Any]] = {}

    for param in parameters:
        if param["in"] != param_type:
            continue

        param_name = param["name"]
        required = param.get("required", False)
        schema = param.get("schema", {})
        type_name = model_name + create_type_name("parameter")
        param_type = resolve_schema_type(type_name, schema, openapi_spec, model_classes)

        title = schema.get("description", None)
        if not title:
            title = get_type_title(param_type, model_classes)

        if required:
            field = Field(..., title=title)
        else:
            field = Field(None, title=title)

        fields[param_name] = (param_type, field)

    return create_model(model_name, **fields)


def endpoint_data_content_to_model(
    model_name: str,
    content: Dict[str, Any],
    required: bool,
    openapi_spec: Dict[str, Any],
    model_classes: _ModelTypeMap,
    optional_children: bool = False,
) -> Type[Any]:
    """
    Create a Pydantic model class from endpoint data content.

    :param model_name: Pydantic model class name.
    :param content: Different endpoint data content schema's from the OpenAPI spec.
    :param required: Content is required.
    :param openapi_spec: The OpenAPI data containing the endpoint information.
    :param model_classes: A dictionary of Pydantic model classes.
    :param optional_children: When True all children will be optional recursively.
    :return: A Pydantic model class.
    """
    fields: Dict[str, Tuple[str, Any]] = {}

    for schema_type, status_content in content.items():
        if schema_type != "application/json":
            continue
        schema = status_content.get("schema", {})
        content_type = resolve_schema_type(
            model_name,
            schema,
            openapi_spec,
            model_classes,
            force_optional=optional_children,
        )

        title = schema.get("description", None)
        if not title:
            title = get_type_title(content_type, model_classes)

        if required:
            field = Field(..., title=title)
        else:
            field = Field(None, title=title)

        fields["content"] = (content_type, field)
        break

    return create_model(model_name, **fields)


def add_endpoint_models(
    openapi_spec: Dict[str, Any],
    method: str,
    path: str,
    model_classes: _ModelTypeMap,
) -> Dict[str, str]:
    """
    Add request and response model classes for a given API endpoint.

    :param openapi_spec: The OpenAPI data containing the endpoint information.
    :param method: The HTTP method (e.g., 'POST', 'GET').
    :param path: The HTTP path (e.g. '/slurm/v0.0.41/jobs').
    :param model_classes: A dictionary of Pydantic model classes.
    :return: The Pydantic model class names for endpoint parameters and response codes.
    """
    openapi_paths = openapi_spec.get("paths", {})
    if path in openapi_paths:
        openapi_path = path
    elif path + "/" in openapi_paths:
        openapi_path = path + "/"
    else:
        raise RuntimeError(f"Endpoint path {path} not found in OpenAPI spec.")

    method_data = openapi_paths[openapi_path].get(method.lower())
    if not method_data:
        raise RuntimeError(f"Method {method} not found for endpoint path {path}.")

    title_prefix = f"{method.upper()} {path}"
    summary = method_data.get("summary")
    parameters = method_data.get("parameters", [])
    body = method_data.get("requestBody", {})
    responses = method_data.get("responses", {})

    model_names = {}

    # Request path parameters

    model_name = create_type_name(method, "path", *path.split("/"))
    model_class = endpoint_parameters_to_model(
        model_name, parameters, "path", openapi_spec, model_classes
    )
    model_name = model_class.__name__
    assert model_name not in model_classes

    title = f"{title_prefix}: path parameters"
    model_classes[model_name] = model_class, title
    model_names["path"] = model_name

    # Request query parameters

    model_name = create_type_name(method, "query", *path.split("/"))
    model_class = endpoint_parameters_to_model(
        model_name,
        parameters,
        "query",
        openapi_spec,
        model_classes,
    )
    model_name = model_class.__name__
    assert model_name not in model_classes
    model_names["query"] = model_name

    if model_name not in model_classes:
        title = f"{title_prefix}: query parameters"
        model_classes[model_name] = model_class, title

    # Request body

    model_name = create_type_name(method, "body", *path.split("/"))
    model_class = endpoint_data_content_to_model(
        model_name,
        body.get("content", {}),
        False,
        openapi_spec,
        model_classes,
    )
    model_name = model_class.__name__
    model_names["body"] = model_name

    if model_name not in model_classes:
        title = f"{title_prefix}: body"
        title = body.get("description", None) or title
        model_classes[model_name] = model_class, title

    # Request responses for different HTTP error codes

    for status_code, response in responses.items():
        status_code = str(status_code)
        assert status_code not in model_names

        model_name = create_type_name(method, "response", status_code, *path.split("/"))
        model_class = endpoint_data_content_to_model(
            model_name,
            response.get("content", {}),
            True,
            openapi_spec,
            model_classes,
            optional_children=True,
        )
        model_name = model_class.__name__
        model_names[status_code] = model_name

        if model_name not in model_classes:
            if summary:
                title = f"{title_prefix}: response {status_code} ({summary})"
            else:
                title = f"{title_prefix}: response {status_code}"
            title = response.get("description", title)
            model_classes[model_name] = model_class, title

    return model_names


def map_versions_to_files(
    openapi_files: List[str],
) -> Dict[Tuple[_VersionType, _VersionType], str]:
    """
    Map OpenAPI version ranges to corresponding file paths.

    :param openapi_files: A list of OpenAPI file paths.
    :return: A ordered dictionary with the same order as `openapi_files`
             which maps version ranges to OpenAPI file paths.
    """
    version_file_map = OrderedDict()
    for openapi_file in openapi_files:
        base_name = os.path.basename(openapi_file).rsplit(".", maxsplit=1)[0]
        start_version_str, end_version_str = base_name.split("_")
        start_version = slurm_access.parse_version(start_version_str)
        end_version = slurm_access.parse_version(end_version_str)
        version_file_map[(start_version, end_version)] = openapi_file
    return version_file_map


def create_endpoint_models(
    openapi_files: List[str], endpoints: List[Tuple[str, str]]
) -> Tuple[Dict[str, _ModelTypeMap], Dict[Tuple[str, str], Tuple[str, Dict[str, str]]]]:
    """
    Generate model classes from multiple OpenAPI files for specific endpoints.

    :param openapi_files: A list of OpenAPI file paths.
    :param endpoints: A list of tuples containing the HTTP method and  path.
    :return: A directory of Pydantic model classes grouped per API version and a
             directory that maps (method, path) tuples to (version, model name) tuples.
    """
    version_file_map = map_versions_to_files(openapi_files)

    endpoints_by_version = defaultdict(list)
    for method, path in endpoints:
        version_str = path.split("/")[2]
        version = slurm_access.parse_version(version_str)
        endpoints_by_version[version].append((method, path))

    all_openapi_spec = dict()

    model_classes: Dict[str, _ModelTypeMap] = dict()
    endpoint_model_names: Dict[Tuple[str, str], Tuple[str, Dict[str, str]]] = dict()
    for version, version_endpoints in endpoints_by_version.items():
        version_str = "v" + "_".join(map(str, version))
        openapi_file = find_file_for_version(version, version_file_map)

        openapi_spec = all_openapi_spec.get(openapi_file)
        if openapi_spec is None:
            with open(openapi_file, "r") as f:
                openapi_spec = json.load(f)
                all_openapi_spec[openapi_file] = openapi_spec

        assert openapi_spec

        version_models = model_classes[version] = dict()
        for method, path in version_endpoints:
            model_names = add_endpoint_models(
                openapi_spec, method, path, version_models
            )
            endpoint_model_names[(method, path)] = version_str, model_names
    return model_classes, endpoint_model_names


def save_endpoint_models(
    models_per_version: Dict[str, _ModelTypeMap], root_dir: str
) -> None:
    for version, version_models in models_per_version.items():
        suffix = "_".join(map(str, version))
        basename = f"v{suffix}.py"
        file_path = os.path.abspath(
            os.path.join(
                root_dir, "..", "src", "pyslurmutils", "client", "rest", "api", basename
            )
        )
        api_model = create_coumpound_model("AllModels", version_models)
        save_model(api_model, file_path)
        print(f"Version {version}: {file_path}")


def create_coumpound_model(
    model_name: str, model_classes: _ModelTypeMap
) -> Type[BaseModel]:
    """
    Create a model with required as fields.

    :param model_name: A dictionary of Pydantic model names.
    :return: A Pydantic model class.
    """
    _fields: Dict[str, Tuple[str, Any]] = {}
    for i, (field_name, (model_class, title)) in enumerate(model_classes.items()):
        field_name = f"model{i}"
        _fields[field_name.lower()] = (model_class, Field(..., title=title))
    return create_model(model_name, **_fields)


def autogenerated_notice() -> str:
    return """# This file is automatically generated.
# DO NOT MODIFY THIS FILE MANUALLY.

"""


def save_model(model_class: Type[BaseModel], file_path: str) -> None:
    """
    Save a Pydantic model class in JSON.

    :param model_class: A Pydantic model class.
    :param file_path: The file where the model will be saved.
    """
    with open(file_path, "w") as f:
        code = generate_model_code(model_class.schema_json(indent=2))
        f.write(autogenerated_notice())
        f.write(code)


def generate_import_code(
    endpoint_model_names: Dict[Tuple[str, str], Tuple[str, Dict[str, str]]],
) -> str:
    code = "ENDPOINTS = {\n"
    for (method, path), (module_name, class_names) in endpoint_model_names.items():
        code += f'    ("{method}", "{path}"): (\n'
        code += f'        "{module_name}",\n'
        code += "        {\n"
        for name, class_name in class_names.items():
            code += f'            "{name}": "{class_name}",\n'
        code += "        },\n"
        code += "    ),\n"
    code += "}\n"
    return code


def save_import_code(code: str, root_dir) -> None:
    file_path = os.path.abspath(
        os.path.join(
            root_dir,
            "..",
            "src",
            "pyslurmutils",
            "client",
            "rest",
            "api",
            "slurm_endpoints.py",
        )
    )
    with open(file_path, "w") as f:
        f.write(autogenerated_notice())
        f.write(code)
    print(f"Endpoints: {file_path}")


if __name__ == "__main__":
    root_dir = os.path.dirname(__file__)

    # Most recent file first:
    openapi_files = [
        os.path.join(root_dir, "v0.0.39_v0.0.41.json"),
    ]

    endpoints = []
    for version in ["v0.0.41", "v0.0.40", "v0.0.39"]:
        for method, path_suffix in [
            ("POST", "job/submit"),
            ("GET", "job/{job_id}"),
            ("DELETE", "job/{job_id}"),
            ("GET", "jobs"),
        ]:
            endpoints.append((method, f"/slurm/{version}/{path_suffix}"))

    models_per_version, endpoint_model_names = create_endpoint_models(
        openapi_files, endpoints
    )
    save_endpoint_models(models_per_version, root_dir)

    code = generate_import_code(endpoint_model_names)
    save_import_code(code, root_dir)
