import os
import json
import logging

from pyslurmutils.client.rest.api import slurm_access
from pyslurmutils.client.rest.api import slurm_response


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    base_url = os.environ["SLURM_URL"]
    user = os.environ["SLURM_USER"]
    token = os.environ["SLURM_TOKEN"].replace("SLURM_JWT=", "").rstrip()

    headers = {"X-SLURM-USER-NAME": user, "X-SLURM-USER-TOKEN": token}

    openapi_spec = slurm_access.slurm_request(
        "GET", base_url, "/openapi", headers=headers
    )
    versions = slurm_response.extract_slurm_api_versions(openapi_spec)

    newest_version = versions[0]
    oldest_version = versions[-1]
    newest_version_str = slurm_access.create_version(newest_version)
    oldest_version_str = slurm_access.create_version(oldest_version)

    root_dir = os.path.dirname(__file__)
    file_path = os.path.join(
        root_dir, f"{oldest_version_str}_{newest_version_str}.json"
    )
    with open(file_path, "w") as f:
        json.dump(openapi_spec, f, indent=2)
