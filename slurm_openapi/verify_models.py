import os
import time
import logging
from typing import List, Dict

from pydantic import BaseModel

# Only import the API, nothing else from pyslurmutils!
from pyslurmutils.client.rest.api import slurm_access
from pyslurmutils.client.rest.api import slurm_response


def submit_job(
    version: str, headers: Dict[str, str], base_url: str, shell_command: str
) -> BaseModel:
    body = {
        "job": {
            "current_working_directory": "/tmp",
            "name": f"pyslurmutils.test.{version}",
            "standard_input": "/dev/null",
            "standard_output": f"/tmp_14_days/pyslurmutils.test.{version}%j.outerr",
        }
    }

    script = f"#!/bin/bash -l\n{shell_command}"
    if slurm_access.parse_version(version) < slurm_access.parse_version("v0.0.39"):
        body["script"] = script
        body["job"]["environment"] = {"_DUMMY_VAR": "dummy_value"}
    else:
        body["job"]["script"] = script
        body["job"]["environment"] = ["_DUMMY_VAR=dummy_value"]

    return slurm_access.validated_slurm_request(
        "POST", base_url, f"/slurm/{version}/job/submit", json=body, headers=headers
    )


def wait_job_state(
    version: str, headers: Dict[str, str], base_url: str, job_id: int, states: List[str]
) -> BaseModel:
    path_params = {"job_id": job_id}
    while True:
        response = slurm_access.validated_slurm_request(
            "GET",
            base_url,
            f"/slurm/{version}/job/{{job_id}}",
            path_params=path_params,
            headers=headers,
        )
        job = response.jobs[0]
        job_state = slurm_response.slurm_job_state(job)
        if job_state in states:
            print(f"\nSLURM job {job.job_id} {job_state}\n")
            return job
        else:
            print(f"\nSLURM job {job.job_id} {job_state}\n")
        time.sleep(0.5)


def cancel_job(
    version: str, headers: Dict[str, str], base_url: str, job_id: int
) -> BaseModel:
    path_params = {"job_id": job_id}
    return slurm_access.validated_slurm_request(
        "DELETE",
        base_url,
        f"/slurm/{version}/job/{{job_id}}",
        path_params=path_params,
        headers=headers,
    )


def list_jobs(
    version: str,
    headers: Dict[str, str],
    base_url: str,
    use: str,
    cause_warning: bool = False,
) -> None:
    if cause_warning:
        # Since v0.0.40 this causes a warning:
        # Ignoring unknown field "dummy" of type string in DATA_PARSER_OPENAPI_JOB_INFO_QUERY
        query_params = {"dummy": "dummy"}
    else:
        query_params = None

    response = slurm_access.validated_slurm_request(
        "GET",
        base_url,
        f"/slurm/{version}/jobs",
        query_params=query_params,
        headers=headers,
    )

    lines = [
        [
            job.name,
            job.job_id,
            job.partition,
            job.job_state,
            slurm_response.slurm_unix_timestamp(job.submit_time),
            slurm_response.slurm_unix_timestamp(job.end_time),
        ]
        for job in response.jobs
        if job.user_name == user
    ]
    lines.insert(0, ["ID", "Name", "Partition", "State", "Submit", "End"])
    print_table(lines)


def print_table(lines: List[str]) -> None:
    col_widths = [
        max(len(str(row[col])) for row in lines) for col in range(len(lines[0]))
    ]
    lines.insert(1, ["-" * w for w in col_widths])
    for line in lines:
        print("  ".join(f"{str(cell):<{col_widths[i]}}" for i, cell in enumerate(line)))


def print_job_logs(job: BaseModel) -> None:
    try:
        with open(job.standard_output, "r") as f:
            print(f.read())
    except FileNotFoundError:
        print()
        print(f"Log file not found: {job.standard_output}")
        print()


def main(version: str, headers: Dict[str, str], base_url: str, user: str) -> None:
    finished_states = "FAILED", "COMPLETED", "CANCELLED", "TIMEOUT"
    running_states = ("RUNNING",)

    print()
    print(f"########## SLURM API {version} ##########")
    print()

    response = submit_job(version, headers, base_url, "ls")
    job = wait_job_state(version, headers, base_url, response.job_id, finished_states)
    print_job_logs(job)

    response = submit_job(version, headers, base_url, "sleep 10")
    job = wait_job_state(version, headers, base_url, response.job_id, running_states)
    _ = cancel_job(version, headers, base_url, response.job_id)
    job = wait_job_state(version, headers, base_url, response.job_id, finished_states)
    print_job_logs(job)

    list_jobs(version, headers, base_url, user)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)

    base_url = os.environ["SLURM_URL"]
    user = os.environ["SLURM_USER"]
    token = os.environ["SLURM_TOKEN"].replace("SLURM_JWT=", "").rstrip()
    api_version = os.environ.get("SLURM_API_VERSION")

    headers = {"X-SLURM-USER-NAME": user, "X-SLURM-USER-TOKEN": token}

    openapi_spec = slurm_access.slurm_request(
        "GET", base_url, "/openapi", headers=headers
    )
    versions = slurm_response.extract_slurm_api_versions(openapi_spec)

    if api_version:
        api_version = slurm_access.parse_version(api_version)
        assert api_version in versions, versions
        versions = [api_version]

    for version in versions:
        version = slurm_access.create_version(version)
        main(version, headers, base_url, user)
