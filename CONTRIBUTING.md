## Environment variables

To develop against a production server, set these enviroment variables

```bash
export SLURM_URL=...
export SLURM_USER=${USER}
export SLURM_TOKEN=$(scontrol token lifespan=3600)
export SLURM_API_VERSION="v0.0.41"  # optional
```

The `scontrol` command needs to be run on a Slurm host.

## Add REST API versions

Extract the OpenAPI specs from a production server

```bash
python ./slurm_openapi/fetch_specs.py
```

Generate the Pydantic models from the OpenAPI specs for the supported endpoints

```bash
python ./slurm_openapi/generate_models.py
```

Test the REST API for all supported versions against a production server

```bash
python ./slurm_openapi/verify_models.py
```

## Unit tests

Tests make use of [pytest](https://docs.pytest.org/en/stable/index.html) and can be run as follows

```bash
pytest . [--slurm-api-version v0.0.41]
```

Testing an installed project is done like this

```bash
pytest --pyargs <project_name> [--slurm-api-version v0.0.41]
```

Testing against a production server is done like this

```bash
pytest . -xv --slurm-root-directory "/tmp_14_days" [--slurm-api-version v0.0.41]
```

## Test CLI

Testing against a production server using the CLI

```bash
pyslurmutils check
pyslurmutils submit "ls"
```

## Test examples

Test documentation examples against a production server using the CLI

```bash
python doc/examples/preemptive.py --slurm-root-directory=/tmp_14_days
python doc/examples/executor.py slurm --slurm-root-directory=/tmp_14_days
```

## Test without pyslurmutils

Test the REST API directly from the shell

```bash
curl -X POST "${SLURM_URL}/slurm/v$(pyslurmutils version)/job/submit" \
    -H "X-SLURM-USER-NAME: ${SLURM_USER}" \
    -H "X-SLURM-USER-TOKEN: ${SLURM_TOKEN}" \
    -H "Content-Type: application/json" \
    -d '{
    "job": {
        "environment": [
            "_DUMMY_VAR=dummy_value"
        ],
        "name": "pyslurmutils.test",
        "standard_input": "/dev/null",
        "standard_output": "/tmp_14_days/pyslurmutils.test.%j.outerr",
        "script": "#!/bin/bash -l\nls",
        "current_working_directory": "/tmp_14_days"
    }
    }'
```

Get job status

```bash
curl -X GET "${SLURM_URL}/slurm/v$(pyslurmutils version)/job/${JOBID}" \
    -H "X-SLURM-USER-NAME: ${SLURM_USER}" \
    -H "X-SLURM-USER-TOKEN: ${SLURM_TOKEN}"
```
