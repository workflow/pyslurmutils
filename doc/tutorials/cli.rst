Command line interface
======================

To use the CLI, three authentication parameters are required

.. code:: bash

  pyslurmutils <command> --url=... --user=... --token=...

Alternatively they can be provided as environment variables

.. code:: bash

  export SLURM_URL=...
  export SLURM_USER=...
  export SLURM_TOKEN=...
  export SLURM_API_VERSION="v0.0.41"  # optional

  pyslurmutils <command>

Check token and client-server API compatibility

.. code:: bash

  pyslurmutils check

List all my recent jobs (e.g. monitor with a 1 second interval)

.. code:: bash

  pyslurmutils status -m 1

Cancel jobs

.. code:: bash

  pyslurmutils cancel 5942999 5943022