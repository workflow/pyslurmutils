Unit tests
==========

Run unit tests locally

.. code:: bash

    pip install pyslurmutils[test]
    pytest --pyargs pyslurmutils [--slurm-api-version v0.0.41]

To run unit tests using SLURM you need three environment variables
and the `pytest` argument `--slurm-root-directory`

.. code:: bash

    export SLURM_TOKEN=$(scontrol token lifespan=3600)
    export SLURM_URL=...
    export SLURM_USER=...
    export SLURM_API_VERSION="v0.0.41"  # optional

    pytest --pyargs pyslurmutils --slurm-root-directory /tmp_14_days [--slurm-api-version v0.0.41]
