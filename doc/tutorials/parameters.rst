Job parameters
==============

When instantiating a SLURM client or executor, you can provide the :code:`parameters` argument

.. code:: python

    from pyslurmutils.client import SlurmScriptRestClient

    parameters={"time_limit": "02:00:00"}

    client = SlurmScriptRestClient(
        url=url,
        user_name=user_name,
        token=token,
        log_directory=log_directory,
        parameters=parameters,
    )

Documentation on all available SLURM job parameters can be found `here <https://slurm.schedmd.com/rest_api.html>`_.

The most commonly used ones are

* :code:`current_working_directory`: working directory where the script will be executed on SLURM
* :code:`time_limit`: limits the job time (hh:mm:ss format)
* :code:`partition`: SLURM partition to which the job should be submitted
* :code:`cpus_per_task`: number of CPU's available to the job
* :code:`memory_per_cpu`: amount of memory per CPU
* :code:`gpus_per_node`: number of GPU's available to the job (make sure the partition has nodes with GPU's)

For example this script

.. code:: python

    from pyslurmutils.client import SlurmScriptRestClient

    SCRIPT = """#!/usr/bin/env python3
    import os
    host = os.gethostname()
    pid = os.gedpid()
    ncpus = len(os.sched_getaffinity(pid))
    print(f'{host=}, {pid=}, {ncpus=}')
    """

    parameters={"cpus_per_task": 4}

    client = SlurmScriptRestClient(
        url=url,
        user_name=user_name,
        token=token,
        log_directory=log_directory,
        parameters=parameters,
    )

    job_id = client.submit_script(SCRIPT)
    try:
        print(client.wait_finished(job_id))
        client.print_stdout_stderr(job_id)
    finally:
        client.clean_job_artifacts(job_id)

The output confirm that 4 CPUs are available to the job

.. code:: bash

    COMPLETED

    STDOUT/STDERR: /tmp_14_days/<username>/slurm_logs/pyslurmutils.<hostname>.15120368.outerr
    -------------------------------------------------------------------------------------
    host='<slurmnode>', pid=1774392, ncpus=4
