Advanced Examples
=================

Advanced executor usage
-----------------------

.. literalinclude:: ../examples/executor.py
   :language: python

Pre-emptive job scheduling
--------------------------

.. literalinclude:: ../examples/preemptive.py
   :language: python
