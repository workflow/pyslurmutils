"""Advanced example illustrating pre-emptive job scheduling.

.. code:: bash

    export SLURM_TOKEN=$(scontrol token lifespan=3600)
    export SLURM_API_VERSION="v0.0.41"  # optional
    export SLURM_URL=...
    export SLURM_USER=...

    python preemptive.py --slurm-root-directory=/tmp_14_days
"""

import os
import sys
import time
import logging
import getpass
import argparse

from pyslurmutils.concurrent.futures import SlurmRestExecutor

logger = logging.getLogger(__name__)


def main(slurm_root_directory, log_level):
    logging.basicConfig(level=log_level, stream=sys.stdout)

    url = os.environ.get("SLURM_URL")
    token = os.environ.get("SLURM_TOKEN")
    user_name = os.environ.get("SLURM_USER", getpass.getuser())

    log_directory = os.path.join(slurm_root_directory, user_name, "slurm_logs")

    with SlurmRestExecutor(
        url=url,
        token=token,
        user_name=user_name,
        log_directory=log_directory,
        lazy_scheduling=False,
        max_workers=2,
        max_tasks_per_worker=10,
        parameters={"time_limit": "00:01:00"},
    ) as executor:

        logger.info("execute a job ...")
        assert executor.submit(sum, [1, 1]).result() == 2

        logger.info("wait longer than the job time limit ...")
        time.sleep(120)

        logger.info("execute a job ...")
        assert executor.submit(sum, [1, 1]).result() == 2


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Jobs stay alive within the executor context."
    )
    parser.add_argument(
        "--slurm-root-directory",
        type=str,
        required=True,
        help="For logs files.",
    )
    parser.add_argument(
        "--log-level",
        type=lambda s: getattr(logging, s.upper()),
        default=logging.INFO,
        help="For logs.",
    )

    args = parser.parse_args()
    main(args.slurm_root_directory, args.log_level)
